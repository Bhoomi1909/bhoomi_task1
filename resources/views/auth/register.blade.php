@extends('user::layouts.master')
@section('content')
<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
    <div class="container">
        <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
            <div class="col-first">
                <h1>Login</h1>
                <nav class="d-flex align-items-center">
                    <a href="{{route('home')}}">Home<span class="lnr lnr-arrow-right"></span></a>
                    <a href="">Register</a>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->
<section class="login_box_area section_gap">
    <div class="container">
        <div class="row">
            <h3>register in to enter</h3>
            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" class="row login_form"
                id="contactForm">
                @csrf
                <div class="col-md-6 form-group">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Username"
                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Username'">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <input type="email" class="form-control" id="name" name="email" placeholder="Email"
                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <input type="password" class="form-control" id="name" name="password" placeholder="Password"
                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <input type="password" class="form-control" id="name" name="password_confirmation"
                        placeholder="confirm Password" onfocus="this.placeholder = ''"
                        onblur="this.placeholder = 'password_confirmation'">
                    @error('passwpassword_confirmationord')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-6 form-group">
                    <input type="File" class="form-control" id="name" name="image" placeholder="image"
                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-12 form-group">
                    <button type="submit" value="submit" class="primary-btn">Register</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    </div>
</section>
@endsection