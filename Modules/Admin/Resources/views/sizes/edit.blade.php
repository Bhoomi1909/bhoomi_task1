@extends('admin::layouts.master')
@section('content')
<div class="header">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fas fa-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Size</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="#" class="btn btn-sm btn-neutral">New</a>
                    <a href="#" class="btn btn-sm btn-neutral">Filters</a>
                </div>
            </div>
            <!-- Card stats -->

        </div>
    </div>
</div>
<div class="container-fluid col-xl-8 ">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-2" class="text-center">
                        <Table> Size Table </Table>
                    </h3>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card-body">
                <form method="POST" action="{{route('size.update',['id'=>$size->id])}}">
                    @csrf
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Size name</label>
                                    <input type="number" class="form-control" placeholder="size name" name="name"
                                        value={{$size->name}}>
                                </div>
                            </div>
                        </div>
                        <button type="reset" class="btn btn-outline-primary">reset</button>

                        <button type="submit" name="submit" class="btn btn-outline-success">Update</button>

                        <button type="button" class="btn btn-outline-info"> View Records</button>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection