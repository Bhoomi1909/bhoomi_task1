@extends('admin::layouts.master')
@section('content')
<div class="container">
    <div class="header-body">
        <div class="row align-items-center py-4">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                </ol>
            </nav>
        </div>
        <!-- Card stats -->
        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                    <a href="{{route('productmasters')}}">
                        <!-- Card body -->
                        <div class="card-body bg-red text-center">
                            <div class="col xl-2">
                                <div class="col-auto ">
                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                        <i class="ni ni-basket"></i>
                                    </div>
                                </div>
                                <h5 class="card-title text-uppercase text-white mb-0">Product Master </h5>
                                <?php $count = DB::table('productmasters')->count();?>
                                <span class="h2 text-white mb-0">{{$count}}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                    <a href="{{route('brands')}}">
                        <!-- Card body -->
                        <div class="card-body bg-blue text-center">
                            <div class="col xl-2">
                                <div class="col-auto ">
                                    <div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow">
                                        <i class="ni ni-tag"></i>
                                    </div>
                                </div>
                                <h5 class="card-title text-uppercase text-white mb-0"> Brand </h5>
                                <?php $count = DB::table('brands')->count();?>
                                <span class="h2 text-white mb-0">{{$count}}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                    <a href="{{route('categories')}}">
                        <!-- Card body -->
                        <div class="card-body bg-green text-center">
                            <div class="col xl-2">
                                <div class="col-auto ">
                                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                        <i class="ni ni-bullet-list-67"></i>
                                    </div>
                                </div>
                                <h5 class="card-title text-uppercase text-white mb-0">category</h5>
                                <?php $count = DB::table('categories')->count();?>
                                <span class="h2 text-white mb-0">{{$count}}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                    <a href="{{route('colors')}}">
                        <!-- Card body -->
                        <div class="card-body bg-orange text-center">
                            <div class="col xl-2">
                                <div class="col-auto ">
                                    <div class="icon icon-shape bg-gradient-orange text-white">
                                        <i class="ni ni-palette "></i>
                                    </div>
                                </div>
                                <h5 class="card-title text-uppercase text-white mb-0"> color </h5>
                                <?php $count = DB::table('colors')->count();?>
                                <span class="h2 text-white mb-0">{{$count}}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                    <a href="{{route('areas')}}">
                        <!-- Card body -->
                        <div class="card-body bg-yellow text-center">
                            <div class="col xl-2">
                                <div class="col-auto ">
                                    <div class="icon icon-shape bg-gradient-yellow text-white rounded-circle shadow">
                                        <i class="ni ni-pin-3 "></i>
                                    </div>
                                </div>
                                <h5 class="card-title text-uppercase text-white mb-0"> area </h5>
                                <?php $count = DB::table('areas')->count();?>
                                <span class="h2 text-white mb-0">{{$count}}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                    <a href="{{route('sizes')}}">
                        <!-- Card body -->
                        <div class="card-body bg-orange text-center">
                            <div class="col xl-2">
                                <div class="col-auto ">
                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                        <i class="ni ni-fat-delete"></i>
                                        <i class="ni ni-fat-add"></i>
                                    </div>
                                </div>
                                <h5 class="card-title text-uppercase text-white mb-0"> size </h5>
                                <?php $count = DB::table('sizes')->count();?>
                                <span class="h2 text-white mb-0">{{$count}}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection