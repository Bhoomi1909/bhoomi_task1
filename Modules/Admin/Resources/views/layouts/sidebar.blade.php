<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->

        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{'dashboard'}}">
                            <i class="ni ni-tv-2 text-primary"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admins')}}">
                            <i class="ni ni-circle-08 text-red"></i>
                            <span class="nav-link-text">Admin</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('areas')}}">
                            <i class="ni ni-pin-3 text-primary"></i>
                            <span class="nav-link-text">Area</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('customers')}}">
                            <i class="ni ni-single-02 text-yellow"></i>
                            <span class="nav-link-text">Customer</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('productmasters')}}">
                            <i class="ni ni-basket text-blue"></i>
                            <span class="nav-link-text">Product Master</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('categories')}}">
                            <i class="ni ni-bullet-list-67 text-orange"></i>
                            <span class="nav-link-text">Category</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('salesorders')}}">
                            <i class="ni ni-cart text-green"></i>
                            <span class="nav-link-text">Sales Order</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('salesorderdetails')}}">
                            <i class="ni ni-align-left-2 text-black"></i>
                            <span class="nav-link-text">Sales Order Details</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('complains')}}">
                            <i class="ni ni-satisfied text-yellow"></i>
                            <span class="nav-link-text">Complain</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('colors')}}">
                            <i class="ni ni-palette text-gray"></i>
                            <span class="nav-link-text">Color</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('brands')}}">
                            <i class="ni ni-tag text-info"></i>
                            <span class="nav-link-text">Brands</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('sizes')}}">
                            <i class="ni ni-fat-delete text-primary"></i>
                            <i class="ni ni-fat-add text-primary"></i>
                            <span class="nav-link-text">Size</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('deliveries')}}">
                            <i class="ni ni-delivery-fast text-danger"></i>
                            <span class="nav-link-text">Delivery</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('invoices')}}">
                            <i class="ni ni-collection text-brown"></i>
                            <span class="nav-link-text">Invoice</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('payments')}}">
                            <i class="ni ni-credit-card text-blue"></i>
                            <span class="nav-link-text">Payment</span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</nav>