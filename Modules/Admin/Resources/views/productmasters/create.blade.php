@extends('admin::layouts.master')
@section('content')
<div class="header">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fas fa-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Product Master</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="#" class="btn btn-sm btn-neutral">New</a>
                    <a href="#" class="btn btn-sm btn-neutral">Filters</a>
                </div>
            </div>
            <!-- Card stats -->
        </div>
    </div>
</div>

<div class="container-fluid col-xl-8 ">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <h3 class="mb-2" class="text-center">
                    <Table> ProductMaster Table </Table>
                </h3>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card-body">
            <form method="POST" action="{{route('productmaster.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label class="form-control-label"> name</label>
                                <input type="text" class="form-control" placeholder="name" name="name">
                                @error('name')
                                <span class="text-danger" style="font-size:13px;font-weight:600">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label class="form-control-label">Details</label>
                                <input type="textarea" class="form-control" placeholder="details" name="details">
                                @error('details')
                                <span class="text-danger" style="font-size:13px;font-weight:600">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">price</label>
                                <input type="text" class="form-control" placeholder="price" name="price">
                                @error('price')
                                <span class="text-danger" style="font-size:13px;font-weight:600">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">stock</label>
                                <input type="text" class="form-control" placeholder="stock" name="stock">
                                @error('stock')
                                <span class="text-danger" style="font-size:13px;font-weight:600">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-10">
                            <div class="form-group">
                                <label class="form-control-label">image</label>
                                <input type="file" class="form-control" placeholder="image" name="image">
                                @error('image')
                                <span class="text-danger" style="font-size:13px;font-weight:600">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="form-control-label">category Id</label>
                            <select id="inputColor" class="form-control" placeholder="category id" name="category_id">
                                <option selected>Choose color</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select> @error('category_id')
                            <span class="text-danger" style="font-size:13px;font-weight:600">
                                {{$message}}
                            </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-4">
                            <label class="form-control-label">size Id</label>
                            <select id="inputColor" class="form-control" placeholder="size id" name="size_id">
                                <option selected>Choose size</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select> @error('size_id')
                            <span class="text-danger" style="font-size:13px;font-weight:600">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-control-label">brand Id</label>
                            <select id="inputColor" class="form-control" placeholder="brand id" name="brand_id">
                                <option selected>Choose brand</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                            @error(' brand_id')
                            <span class="text-danger" style="font-size:13px;font-weight:600">
                                {{$message}}
                            </span>
                            @enderror
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputColor">color Id</label>
                            <select id="inputColor" class="form-control" placeholder="color id" name="color_id">
                                <option selected>Choose color</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                            @error('color_id')
                            <span class="text-danger" style="font-size:13px;font-weight:600">
                                {{$message}}
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="reset" class="btn btn-outline-danger">Reset</button>

                    <button type="submit" class="btn btn-outline-success">Submit</button>

                    <button type="button" class="btn btn-outline-primary">Back</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection