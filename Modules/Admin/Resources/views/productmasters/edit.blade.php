@extends('admin::layouts.master')

@section('content')
<div class="header">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fas fa-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Product MAster</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="#" class="btn btn-sm btn-neutral">New</a>
                    <a href="#" class="btn btn-sm btn-neutral">Filters</a>
                </div>
            </div>
            <!-- Card stats -->

        </div>
    </div>
</div>
<div class="container-fluid col-xl-8 ">
    <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-2" class="text-center">
                        <Table> ProductMaster Table </Table>
                    </h3>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card-body">
                <form method="POST" action="{{route('productmaster.update',['id'=>$productmaster->id])}}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label"> name</label>
                                    <input type="text" class="form-control" placeholder="name" name="name"
                                        value="{{$productmaster->name}}">
                                    @error('name')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Details</label>
                                    <input type="text" class="form-control" placeholder="details" name="details"
                                        value="{{$productmaster->details}}">
                                    @error('details')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">price</label>
                                    <input type="text" class="form-control" placeholder="price" name="price"
                                        value="{{$productmaster->price}}">
                                    @error('price')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">stock</label>
                                    <input type="text" class="form-control" placeholder="stock" name="stock"
                                        value="{{$productmaster->stock}}">
                                    @error('stock')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">category Id</label>
                                    <input type="text" class="form-control" placeholder="category id" name="category_id"
                                        value="{{$productmaster->category_id}}">
                                    @error('category_id')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class=" col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">image</label>
                                    <img src="{{asset('uploads/'.$productmaster->image)}}" width="150px">
                                    <input type="file" class="form-control" placeholder="image" name="image"
                                        value="{{$productmaster->image}}">
                                    @error('image')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">size Id</label>
                                    <input type="text" class="form-control" placeholder="size id" name="size_id"
                                        value="{{$productmaster->size_id}}">
                                    @error('size_id')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">brand Id</label>
                                    <input type="text" class="form-control" placeholder="brand id" name="brand_id"
                                        value="{{$productmaster->brand_id}}">
                                    @error('brand_id')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">color Id</label>
                                    <input type="text" class="form-control" placeholder="color id" name="color_id"
                                        value="{{$productmaster->color_id}}">
                                    @error('color_id')
                                    <span class="text-danger" style="font-size:13px;font-weight:600">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="reset" class="btn btn-outline-primary">reset</button>

                    <button type="submit" name="submit" class="btn btn-outline-success">update</button>

                    <button type="button" class="btn btn-outline-info"> View Records</button>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection