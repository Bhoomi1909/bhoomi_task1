@extends('admin::layouts.master')

@section('content')
<div class="
">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fas fa-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Color</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="#" class="btn btn-sm btn-neutral">New</a>
                    <a href="#" class="btn btn-sm btn-neutral">Filters</a>
                </div>
            </div>
            <!-- Card stats -->

        </div>
    </div>
</div>

<div class="container-fluid mt-0">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="text-center mb-3">
                    <a href="{{route('color.create')}}" class="btn btn-sm btn-primary">Add Color</a>
                </div>
                <!-- Light table -->
                @if(session('success'))
                <div class="alert alert-success text-center">
                    {{session('success')}}
                </div>
                @endif
                <!-- Light table -->
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="name" style="font-size:12px;font-weight:700">ID
                                </th>
                                <th scope="col" class="sort" data-sort="name" style="font-size:12px;font-weight:700">
                                    Name</th>
                                <th scope="col" class="sort" data-sort="name" style="font-size:12px;font-weight:700">
                                    Action</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            @foreach ($colors as $color)
                            <tr>
                                <td scope="row">
                                    <span class="name mb-0 text-sm">{{$loop->iteration}}</span>
                                </td>
                                <td scope="row">
                                    <span class="name mb-0 text-sm">{{$color->name}}</span>
                                </td>
                                <td scope="row">
                                    <a href="{{route('color.edit',['id'=>$color->id])}}"
                                        class="btn btn-sm btn-success">Edit</a>
                                    <a href="{{route('color.delete',['id'=>$color->id])}}"
                                        class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- Card footer -->
                <div class="card-footer py-4">
                    <nav aria-label="...">
                        <ul class="pagination justify-content-end mb-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">
                                    <i class="fas fa-angle-left"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">
                                    <i class="fas fa-angle-right"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection