@extends('admin::layouts.masterdisplay')
@extends('admin::layouts.sidebar')
@extends('admin::layouts.header')
@extends('admin::layouts.footer')
@section('content')
<div class="col-lg-3 col-md-5">
    <a href="#!" class="btn btn-primary">Add Sales Order</a>
</div>
<div class="card-header border-0">
    <h3 class="mb-0"> Sales Order Table</h3>
</div>
<!-- Light table -->
<div class="table-responsive">
    <table class="table align-items-center table-flush">
        <thead class="thead-light">
            <tr>
                <th scope="col" class="sort" data-sort="name">Customer ID</th>
                <th scope="col" class="sort" data-sort="name">date</th>
                <th scope="col" class="sort" data-sort="name">OrderStatus</th>
            </tr>
        </thead>
        <tbody class="list">
            <tr>
                <td scope="row">
                    <span class="name mb-0 text-sm">Argon Design System</span>
                </td>
                <td scope="row">
                    <span class="name mb-0 text-sm">Argon Design System</span>
                </td>
                <td scope="row">
                    <span class="name mb-0 text-sm">Argon Design System</span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection