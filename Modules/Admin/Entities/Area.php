<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class area extends Model
{
    protected $table = 'areas';
    protected $fillable = [
        'name',
        'pincode',
    ];
}
