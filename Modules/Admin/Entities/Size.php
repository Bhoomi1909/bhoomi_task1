<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class size extends Model
{
    protected $table = 'sizes';
    protected $fillable = [
        'name',
    ];
}
