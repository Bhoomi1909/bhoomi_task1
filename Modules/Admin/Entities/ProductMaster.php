<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductMaster extends Model
{
    protected $table = 'productmasters';
    protected $fillable = [
        'name',
        'details',
        'price',
        'stock',
        'category_id',
        'image',
        'size_id',
        'brand_id',
        'color_id',
    ];
}
