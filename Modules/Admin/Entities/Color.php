<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class color extends Model
{
    protected $table = 'colors';
    protected $fillable = [
        'name',
    ];
}
