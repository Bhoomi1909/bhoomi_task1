<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'name',
    ];
}
