<?php

use Illuminate\Support\Facades\Route;
use Modules\Admin\Http\Controllers\AdminController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->middleware('auth')->group(function () {


    Route::get('/dashboard', function () {
        return view('admin::dashboard');
    })->name('dashboard');

    Route::get('/login', function () {
        return view('admin::login');
    });
    //Route for Admin
    Route::get('/admins', 'AdminController@index')->name('admins');
    Route::get('/admin/create', 'AdminController@create')->name('admin.create');
    Route::post('/admin/store', 'AdminController@store')->name('admin.store');

    //Route for brands
    Route::get('/brands', 'BrandController@index')->name('brands');
    Route::get('/brand/create', 'BrandController@create')->name('brand.create');
    Route::post('/brand/store', 'BrandController@store')->name('brand.store');
    Route::get('/brand/{id}/edit', 'BrandController@edit')->name('brand.edit');
    Route::post('/brand/{id}/update', 'BrandController@update')->name('brand.update');
    Route::get('/brand/{id}/delete', 'BrandController@delete')->name('brand.delete');

    //Route for colors
    Route::get('/colors', 'ColorController@index')->name('colors');
    Route::get('/color/create', 'ColorController@create')->name('color.create');
    Route::post('/color/store', 'ColorController@store')->name('color.store');
    Route::get('/color/{id}/edit', 'ColorController@edit')->name('color.edit');
    Route::post('/color/{id}/update', 'ColorController@update')->name('color.update');
    Route::get('/color/{id}/delete', 'ColorController@delete')->name('color.delete');


    //route for sizes
    Route::get('/sizes', 'SizeController@index')->name('sizes');
    Route::get('/size/create', 'SizeController@create')->name('size.create');
    Route::post('/size/store', 'SizeController@store')->name('size.store');
    Route::get('/size/{id}/edit', 'SizeController@edit')->name('size.edit');
    Route::post('/size/{id}/update', 'SizeController@update')->name('size.update');
    Route::get('/size/{id}/delete', 'SizeController@delete')->name('size.delete');

    //route for category
    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/category/create', 'CategoryController@create')->name('category.create');
    Route::post('/category/store', 'CategoryController@store')->name('category.store');
    Route::get('/category/{id}/edit', 'CategoryController@edit')->name('category.edit');
    Route::post('/category/{id}/update', 'CategoryController@update')->name('category.update');
    Route::get('/category/{id}/delete', 'CategoryController@delete')->name('category.delete');

    //route for Productmaster
    Route::get('/productmasters', 'ProductMasterController@index')->name('productmasters');
    Route::get('/productmaster/create', 'ProductMasterController@create')->name('productmaster.create');
    Route::post('/productmaster/store', 'ProductMasterController@store')->name('productmaster.store');
    Route::get('/productmaster/{id}/edit', 'ProductMasterController@edit')->name('productmaster.edit');
    Route::post('/productmaster/{id}/update', 'ProductMasterController@update')->name('productmaster.update');
    Route::get('/productmaster/{id}/delete', 'ProductMasterController@delete')->name('productmaster.delete');

    //route for Delivery
    Route::get('/deliveries',  'DeliveryController@index')->name('deliveries');
    Route::get('/delivery/create', 'DeliveryController@create')->name('delivery.create');
    Route::post('/delivery/store', 'DeliveryController@store')->name('delivery.store');

    //route for area
    Route::get('/areas', 'areaController@index')->name('areas');
    Route::get('/area/create', 'AreaController@create')->name('area.create');
    Route::post('/area/store', 'AreaController@store')->name('area.store');
    Route::get('/area/{id}/edit', 'AreaController@edit')->name('area.edit');
    Route::post('/area/{id}/update', 'AreaController@update')->name('area.update');
    Route::get('/area/{id}/delete', 'AreaController@delete')->name('area.delete');



    //route for Complain
    Route::get('/complains', 'ComplainController@index')->name('complains');

    //route for salesorders
    Route::get('/salesorders', 'SalesOrderController@index')->name('salesorders');



    //route for SalesOrderDetails
    Route::get('/salesorderdetails', 'SalesOrderDetailsController@index')->name('salesorderdetails');



    //route for Incoices
    Route::get('/invoices', 'InvoicesController@index')->name('invoices');

    //route for Patyment
    Route::get('/payments', 'PaymentController@index')->name('payments');

    //route for customer
    Route::get('/customers', 'PaymentController@index')->name('customers');
});
