<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Entities\Area;
use Illuminate\Routing\Controller;

class AreaController extends Controller
{
    public function index()
    {
        $areas = Area::all();
        return view('admin::areas.index', compact('areas'));
    }

    public function create()
    {
        return view('admin::areas.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'pincode' => 'required|size:5',
        ]);

        $area = new area([
            'name' => $request->name,
            'pincode' => $request->pincode,
        ]);

        $area->save();

        return redirect()->route('areas')->with('success', 'area added successfully');
    }

    public function edit($id)
    {
        $area = area::where('id', $id)->first();
        return view('admin::areas.edit', compact('area'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'pincode' => 'required|size:5',
        ]);

        $area = area::where('id', $id)->first();

        $area->name = $request->name;
        $area->pincode = $request->pincode;

        $area->save();

        return redirect()->route('areas')->with('success', 'area updated successfully');
    }

    public function delete($id)
    {
        area::where('id', $id)->delete();

        return redirect()->route('areas')->with('success', 'area deleted successfully');
    }
}
