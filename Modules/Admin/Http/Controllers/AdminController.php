<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\storage;

class AdminController extends Controller
{
    public function index()
    {
        $admins = user::all();
        return view('admin::admins.index', compact('admins'));
    }

    public function create()
    {
        return view('admin::admins.create');
    }
    public function store(Request $request)
    {


        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'password' => 'required',
            'image' => 'required',

        ]);
        //get file data using file
        $file123 = $request->file('image');
        //file Inbuilt function to
        $extension = $file123->getClientOriginalExtension();
        $mimetype = $file123->getClientMimeType();
        $getfilname = $file123->getFilename();

        //we will use orignal name to upload file
        $original_filename = $file123->getClientOriginalName();

        //upload file on public folder
        Storage::disk('public')->put($original_filename, file::get($file123));

        $admin = new user([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'role' => 1,
            'image' => $original_filename,
        ]);

        $admin->save();

        return redirect()->route('admins')->with('success', 'admin added successfully');
    }
}
