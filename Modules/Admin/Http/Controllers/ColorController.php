<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Entities\Color;
use Illuminate\Routing\Controller;

class ColorController extends Controller
{
    public function index()
    {
        $colors = Color::all();
        return view('admin::colors.index', compact('colors'));
    }

    public function create()
    {
        return view('admin::colors.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:colors',
        ]);

        $color = new Color([
            'name' => $request->name,
        ]);

        $color->save();

        return redirect()->route('colors')->with('success', 'color added successfully');
    }
    public function edit($id)
    {
        $color = Color::where('id', $id)->first();
        return view('admin::colors.edit', compact('color'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:colors',
        ]);
        $color = Color::where('id', $id)->first();
        $color->name = $request->name;

        $color->save();

        return redirect()->route('colors')->with('success', 'color updated successfully');
    }
    public function delete($id)
    {
        Color::where('id', $id)->delete();

        return redirect()->route('colors')->with('success', 'color delete successfully');
    }
}
