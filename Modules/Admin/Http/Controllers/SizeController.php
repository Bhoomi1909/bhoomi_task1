<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Entities\Size;
use Illuminate\Routing\Controller;

class SizeController extends Controller
{
    public function index()
    {
        $sizes = Size::all();
        return view('admin::sizes.index', compact('sizes'));
    }

    public function create()
    {
        return view('admin::sizes.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:11|integer',
        ]);

        $size = new Size([
            'name' => $request->name,
        ]);

        $size->save();

        return redirect()->route('sizes')->with('success', 'size added successfully');
    }

    public function edit($id)
    {
        $size = Size::where('id', $id)->first();
        return view('admin::sizes.edit', compact('size'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|max:11|integer|min:4',
        ]);
        $size = Size::where('id', $id)->first();

        $size->name = $request->name;

        $size->save();

        return redirect()->route('sizes')->with('success', 'size updated successfully');
    }

    public function delete($id)
    {
        Size::where('id', $id)->delete();

        return redirect()->route('sizes')->with('success', 'size deleted successfully');
    }
}
