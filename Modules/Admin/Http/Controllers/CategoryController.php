<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Admin\Entities\Category;
use Illuminate\http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin::categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin::categories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:categories',
        ]);

        $category = new Category([
            'name' => $request->name,
        ]);

        $category->save();

        return redirect()->route('categories')->with('success', 'category added successfully');
    }
    public function edit($id)
    {
        $category = Category::where('id', $id)->first();
        return view('admin::categories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {
        $category = Category::where('id', $id)->first();

        $category->name = $request->name;

        $category->save();

        return redirect()->route('categories')->with('success', 'category updated successfully');
    }

    public function delete($id)
    {
        Category::where('id', $id)->delete();

        return redirect()->route('categories')->with('success', 'category deleted successfully');
    }
}
