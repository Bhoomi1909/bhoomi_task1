<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Admin\Entities\ProductMaster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\storage;

class ProductMasterController extends Controller
{
    public function index()
    {
        $productmasters = ProductMaster::all();
        return view('admin::productmasters.index', compact('productmasters'));
    }
    public function create()
    {
        return view('admin::productmasters.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'details' => 'required|max:255',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'category_id' => 'required|integer',
            'image' => 'required',
            'size_id' => 'required|integer',
            'brand_id' => 'required|integer',
            'color_id' => 'required|integer',


        ]);
        //get file data using file
        $file123 = $request->file('image');
        //file Inbuilt function to
        $extension = $file123->getClientOriginalExtension();
        $mimetype = $file123->getClientMimeType();
        $getfilname = $file123->getFilename();

        //we will use orignal name to upload file
        $original_filename = $file123->getClientOriginalName();

        //upload file on public folder
        Storage::disk('public')->put($original_filename, file::get($file123));

        $productmaster = new ProductMaster([
            'name' => $request->name,
            'details' => $request->details,
            'price' => $request->price,
            'stock' => $request->stock,
            'category_id' => $request->category_id,
            'image' => $original_filename,
            'size_id' => $request->size_id,
            'brand_id' => $request->brand_id,
            'color_id' => $request->color_id,
        ]);
        $productmaster->save();
        return redirect()->route('productmasters')->with('success', 'product add succesfully');
    }
    public function edit($id)
    {
        $productmaster = ProductMaster::where('id', $id)->first();
        return view('admin::productmasters.edit', compact('productmaster'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'details' => 'required|max:255',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'category_id' => 'required|integer',
            'image' => 'required',
            'size_id' => 'required|integer',
            'brand_id' => 'required|integer',
            'color_id' => 'required|integer',

        ]);
        //First Find product image path to delete
        $productmasters = ProductMaster::where('id', $id)->first();
        $image_path = public_path() . '/uploads/' . $productmasters->image;

        //Delete file using file path
        File::delete($image_path);

        //get file data using file
        $file123 = $request->file('image');

        //file Inbuilt function to
        $extension = $file123->getClientOriginalExtension();
        $mimetype = $file123->getClientMimeType();
        $getfilname = $file123->getFilename();

        //we will use orignal name to upload file
        $original_filename = $file123->getClientOriginalName();

        //upload file on public folder
        Storage::disk('public')->put($original_filename, file::get($file123));


        $productmaster = ProductMaster::where('id', $id)->first();
        $productmaster->name = $request->name;
        $productmaster->details = $request->details;
        $productmaster->price = $request->price;
        $productmaster->stock = $request->stock;
        $productmaster->category_id = $request->category_id;
        $productmaster->image =  $original_filename;
        $productmaster->size_id = $request->size_id;
        $productmaster->brand_id = $request->brand_id;
        $productmaster->color_id = $request->color_id;

        $productmaster->save();

        return redirect()->route('productmasters')->with('success', 'productmaster updated successfully');
    }
    public function delete($id)
    {

        //first find product image path using select queary
        $productmasters = ProductMaster::where('id', $id)->first();
        $image_path = public_path() . '/uploads/' . $productmasters->image;

        //Delete file using file path
        File::delete($image_path);


        ProductMaster::where('id', $id)->delete();

        return redirect()->route('productmasters')->with('success', 'product deleted successfully');
    }
}
