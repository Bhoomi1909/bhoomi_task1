<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Entities\Brand;
use Illuminate\Routing\Controller;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::all();
        return view('admin::brands.index', compact('brands'));
    }

    public function create()
    {
        return view('admin::brands.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:brands',
        ]);

        $brand = new Brand([
            'name' => $request->name,
        ]);

        $brand->save();

        return redirect()->route('brands')->with('success', 'Brand added successfully');
    }

    public function edit($id)
    {
        $brand = Brand::where('id', $id)->first();
        return view('admin::brands.edit', compact('brand'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:brands',
        ]);

        $brand = Brand::where('id', $id)->first();

        $brand->name = $request->name;

        $brand->save();

        return redirect()->route('brands')->with('success', 'Brand updated successfully');
    }

    public function delete($id)
    {
        Brand::where('id', $id)->delete();

        return redirect()->route('brands')->with('success', 'Brand deleted successfully');
    }
}
