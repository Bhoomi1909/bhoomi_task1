
<?php

use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->group(function () {

    Route::get('/home', function () {
        return view('user::home');
    })->name('home');
    Route::get('/contact', function () {
        return view('user::contact');
    })->name('contact');


    Route::get('/productmaster', 'ProductMasterController@index')->name('productmaster');
    Route::get('/productdetails/{id}', 'ProductMasterController@details')->name('productdetails');


    Route::get('/category_products/{id}', 'ProductMasterController@category_products')->name('category_products');
    Route::get('/brand_products/{id}', 'ProductMasterController@brand_products')->name('brand_products');
    Route::get('/color_products/{id}', 'ProductMasterController@color_products')->name('color_products');
    Route::get('/size_products/{id}', 'ProductMasterController@size_products')->name('size_products');
    Route::get('/addtocart/{id}', 'CartController@addtocart')->name('addtocart');
    Route::get('/cart/edit/{id}', 'CartController@edit')->name('cart.edit');
    Route::get('/cart', 'CartController@index')->name('cart');
});
