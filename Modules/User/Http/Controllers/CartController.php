<?php

namespace Modules\User\Http\Controllers;


use Modules\Admin\Entities\ProductMaster;
use Illuminate\Routing\Controller;
use Gloudemans\Shoppingcart\Facedes\Cart;


class CartController extends Controller
{

    public function addtocart($id)
    {

        $productmasters = ProductMaster::where('id', $id)->get();
        return view('user::cart', compact('productmasters'));
    }
    public function index($id)
    {

        $productmasters = Cart::content();
        return view('cart', compact('productmasters'));
    }
    public function edit($id)
    {

        $productmasters = ProductMaster::find($id);
        Cart::add($productmasters->id, $productmasters->name, $productmasters->price);
    }
}
