<?php

namespace Modules\User\Http\Controllers;

use GuzzleHttp\Psr7\Request;
use Illuminate\Routing\Controller;
use Modules\Admin\Entities\ProductMaster;
use Modules\Admin\Entities\category;
use Illuminate\Support\Facades\DB;

class ProductMasterController extends Controller
{
    public function index()
    {
        $productmasters = ProductMaster::all();
        return view('user::productmaster', compact('productmasters'));
    }
    public function details($id)
    {
        $productmasters = ProductMaster::where('id', $id)->get();
        return view('user::productdetails', compact('productmasters'));
    }

    public function category_products($id)
    {
        $productmasters = ProductMaster::where('category_id', $id)->get();
        return view('user::productmaster', compact('productmasters'));
    }
    public function brand_products($id)
    {

        $productmasters = ProductMaster::where('brand_id', $id)->get();
        return view('user::productmaster', compact('productmasters',));
    }
    public function color_products($id)
    {

        $productmasters = ProductMaster::where('color_id', $id)->get();
        return view('user::productmaster', compact('productmasters',));
    }
}
