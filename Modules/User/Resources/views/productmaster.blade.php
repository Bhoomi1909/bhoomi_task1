@extends('user::layouts.master')
@section('content')

<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
    <div class="container">
        <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
            <div class="col-first">
                <h1>Shop Category page</h1>
                <nav class="d-flex align-items-center">
                    <a href="{{route('home')}}">Home<span class="lnr lnr-arrow-right"></span></a>
                    <a href="#">Shop</a>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-3 col-lg-4 col-md-5">
            <div class="sidebar-filter mt-50">
                <div class="sidebar-categories">
                    <div class="head">Product brand</div>
                    <ul class="main-categories">
                        <li class="main-nav-list">
                            <ul>
                                <?php $brands = DB::table('brands')->get();?>
                                @foreach($brands as $brand)
                                <li class="filter-list" value="{{$brand->id}}"><a
                                        href="{{route('brand_products',$brand->id)}}">{{$brand->name}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="sidebar-categories">
                    <div class="head"> Product Categories</div>
                    <ul class="main-categories">
                        <li class="main-nav-list">
                            <ul>
                                <?php $cats = DB::table('categories')->get();?> @foreach($cats as $cat) <li
                                    class="main-nav-list child" value="{{$cat->id}}"><a
                                        href="{{route('category_products',$cat->id)}}">{{$cat->name}}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="sidebar-categories">
                    <div class="head">product color</div>
                    <ul class="main-categories">
                        <li class="main-nav-list">
                            <ul>
                                <?php $colors = DB::table('colors')->get();?>
                                @foreach($colors as $color)
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="black"
                                        name="color"><label for="black" value="{{$color->id}}"><a
                                            href="{{route('color_products',$color->id)}}"><span>{{$color->name}}</label></a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-8 col-md-7">
            <!-- Start Filter Bar -->
            <div class="filter-bar d-flex flex-wrap align-items-center">
                <div class="sorting">
                    <select>
                        <option value="1">Default sorting</option>
                        <option value="1">Default sorting</option>
                        <option value="1">Default sorting</option>
                    </select>
                </div>
                <div class="sorting mr-auto">
                    <select>
                        <option value="1">Show 12</option>
                        <option value="1">Show 12</option>
                        <option value="1">Show 12</option>
                    </select>
                </div>
                <div class="pagination">
                    <a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
                    <a href="#" class="active">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                    <a href="#">6</a>
                    <a href="#" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
            <!-- End Filter Bar -->
            <!-- Start Best Seller -->
            <section class="lattest-product-area pb-40 category-list">
                <div class="row">
                    <!-- single product -->

                    @foreach ($productmasters as $productmaster)
                    <div class="col-lg-4 col-md-6">
                        <div class="single-product">
                            <img class="img-fluid" src="{{asset('uploads/'.$productmaster->image)}}" width='50' alt="">
                            <div class="product-details">
                                <h6>{{$productmaster->details}}</h6>
                                <div class="price">
                                    <h6>{{$productmaster->price}}</h6>
                                    <h6 class="l-through">$210.00</h6>
                                </div>
                                <div class="prd-bottom">

                                    <a href="{{route('cart.edit',$productmaster->id)}}" class="social-info">
                                        <span class="ti-bag"></span>
                                        <p class="hover-text">add to cart</p>
                                    </a>
                                    <a href="{{route('productdetails',$productmaster->id)}}" class="social-info">
                                        <span class="lnr lnr-move"></span>
                                        <p class="hover-text">view details</p>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- single product -->
                    @endforeach

                </div>
            </section>
            <!-- End Best Seller -->
            <!-- Start Filter Bar -->

            <!-- End Filter Bar -->
        </div>
    </div>
</div>
@endsection